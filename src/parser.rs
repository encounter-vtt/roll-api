peg::parser! {
    grammar roll() for str {
        use syntax::*;
        rule natural_num() = ['0'..='9']+

        rule lower_case() = ['a'..='z']+

        rule upper_case() = ['A'..='Z']+

        rule alphanumeric() = ['0'..='9' | 'a'..='z' | 'A'..='Z']+

        rule number() -> Value<'input>
            = quiet!{ num:$("-"? natural_num() ("." natural_num())?) } / expected!("number") {
                Value::Number(num.parse().unwrap())
            }

        rule number_set() -> Value<'input>
            = quiet!{ "[" nums:number() ** "," "]" } / expected!("set of numbers") {?
                if !nums.is_empty() {
                    Ok(Value::NumberSet(nums))
                } else {
                    Err("invalid number set - no elements")
                }
            }

        rule variable() -> Value<'input>
            = "@{" (sheet:alphanumeric() "|")? variable:alphanumeric() "}"

        rule roll() -> Roll<'input>
            = dice:natural_num()
            "d"
            sides:(quiet!{ number() / number_set() } / expected!("number or number set")) {
                Roll {
                    mods: vec![],
                    dice,
                    sides,
                }
            }

        rule roll_with_mods() -> Roll<'input>
            = roll:roll()
            mods:(modifier:$(['d', 'e', 'k', 'r', 't',])
                comp:$([">", "<", "==", "!=", ">=", "<="])?
                value:(quiet!{ number() / number_set() } / expected!("number or number set"))
                )* {
                let r = &mut roll;
                for (m, c, v) in mods {
                    let c = match c {
                        ">" => Comparison::Greater,
                        "<" => Comparison::Less,
                        "==" => Comparison::Equal,
                        "!=" => Comparison::NotEquals,
                        ">=" => Comparison::Greater | Comparison::Equal,
                        "<=" => Comparison::Less | Comparison::Equal,
                    };

                    match m {
                        'd' => r.mods.push(Modifier::Drop(c, v)),
                        'e' => r.mods.push(Modifier::Explode(c, v)),
                        'k' => r.mods.push(Modifier::Keep(c, v)),
                        'r' => r.mods.push(Modifier::Reroll(c, v)),
                        't' => r.mods.push(Modifier::Target(c, v)),
                    }
                }
                roll
            }
    }
}
