use bytemuck::bytes_of_mut;
use randomize::PCG32;

use crate::parser;
use crate::syntax::Roll;

pub struct Roller {
    rng: PCG32,
}

pub struct RollResult {
    values: Vec<f32>,

}

impl Roller {
    pub fn new() -> Self {
        let mut init = [0, 0];
        getrandom::getrandom(bytes_of_mut(&mut init)).unwrap();
        let [state, inc] = init;
        let rng = PCG32::seed(state, inc);

        Roller {
            rng,
        }
    }

    pub fn execute_role(&mut self, roll: &Roll<'_>) -> RollResult {
        // self.rng.
    }
}
