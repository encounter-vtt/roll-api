//pub enum Node<'a> {
//    Roll(Roll<'a>),
//    Scope(Vec<Node<'a>>),
//    Infix {
//        left: Node<'a>,
//        op: &'a str,
//        right: Node<'a>,
//    }
//}

pub struct Roll<'a> {
    pub dice: Value<'a>,
    pub mods: Vec<Modifier<'a>>,
    pub sides: Value<'a>,
}

pub enum Value<'a> {
    Number(f32),
    NumberSet(Vec<f32>),
    Raw(&'a str),
    Reference {
        sheet: Option<&'a str>,
        variable: &'a str,
    },
}

pub enum Modifier<'a> {
    Drop(Comparison, Value<'a>),
    Explode(Comparison, Value<'a>),
    Keep(Comparison, Value<'a>),
    Reroll(Comparison, Value<'a>),
    Target(Comparison, Value<'a>),
}

bitflags! {
    struct Comparison: u8 {
        const EQUALS      = 0b00000001,
        const NOTEQUALS   = 0b00000010,
        const GREATER     = 0b00000100,
        const LESS        = 0b00001000,
    }
}
